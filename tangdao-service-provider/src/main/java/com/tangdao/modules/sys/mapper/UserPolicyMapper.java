/**
 *
 */
package com.tangdao.modules.sys.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tangdao.model.domain.UserPolicy;

/**
 * <p>
 * TODO 描述
 * </p>
 *
 * @author ruyang@gmail.com
 * @since 2020年6月5日
 */
@Mapper
public interface UserPolicyMapper extends BaseMapper<UserPolicy>{
	
}
