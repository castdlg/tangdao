/**
 *
 */
package com.tangdao.web.security.user;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * <p>
 * TODO 描述
 * </p>
 *
 * @author ruyang@gmail.com
 * @since 2020年4月29日
 */
public interface IUserDetailsService extends UserDetailsService {

}
