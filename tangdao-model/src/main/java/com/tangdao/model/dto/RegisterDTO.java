/**
 *
 */
package com.tangdao.model.dto;

import com.tangdao.model.domain.User;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * TODO 描述
 * </p>
 *
 * @author ruyang@gmail.com
 * @since 2020年6月12日
 */
@Getter
@Setter
public class RegisterDTO extends User{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
