/**
 *
 */
package com.tangdao.model.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.tangdao.model.base.BaseEntity;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * TODO 描述
 * </p>
 *
 * @author ruyang@gmail.com
 * @since 2020年7月7日
 */
@Getter
@Setter
@TableName("role_policy")
public class RolePolicy extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String roleId;
	
	private String policyId;
}
